<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Collaboration SCRS/NSA/GCHQ/AIC</title>
	</head>
	<body>
		<h1>Accès Restreint</h1>
		<p>L'accès à ce site est réservés aux employés autorisés des agences de renseignement du commonwealth.</p>
		<p>Les pages sont secrètes et non-référencées.</p>
	</body>
</html>

