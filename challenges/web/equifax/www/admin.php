<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Transequiunionfax</title>
	</head>
	<body>
		<h1>Transequiunionfax - Customer Portal</h1>
<?php
$private_info = array("guest"=>"<ul><li>Name: guest</li><li>Job: Chinchilla Walker</li><li>Flag: hahaha No!</li></ul>",
	"admin"=>"<ul><li>Name: admin</li><li>Job: Sysadmin lvl 99</li><li>Flag: UQAM{br0k3n_4uth3nticati0n_is_fun}</li></ul>");

if (!isset($_COOKIE["SESSION"]) || empty($_COOKIE["SESSION"]) || !base64_decode($_COOKIE["SESSION"], TRUE) || count(explode(":", base64_decode($_COOKIE["SESSION"]))) != 2) {
	echo "Invalid session, please log in";
	die();
} else {
	$creds = base64_decode($_COOKIE["SESSION"]);
	list($username, $password) = explode(":", $creds, 2);
	echo "<h2>My Information</h2>\n";

	if (!array_key_exists($username,$private_info) || strlen($password) < 1) {
		echo "Invalid user information, please log in";
		die();
	} else {
		echo $private_info[$username] . "\n";
	}
}
?>
	</body>
</html>

